function [descriptors, varargout] = clustering_batch(images_path, data_path, varargin)
    parser = inputParser;
    parser.KeepUnmatched = true;
    addParameter(parser, 'removeZeroLabel', false, @islogical);
    addParameter(parser, 'shuffleFiles', false, @islogical);
    addParameter(parser, 'livePreview', true, @islogical);
    Clusterer.parserSetup(parser);
    
    parse(parser, varargin{:});
    params = parser.Results;

    params
    
    file_names = images_list_from(images_path);
    
    if params.removeZeroLabel
        file_names = file_names(find(cellfun(@isempty, regexp(file_names, '-0\.jpg$'))));
    end
    
    if params.shuffleFiles
        files_order = randperm(length(file_names));
    else
        files_order = 1:length(file_names);
    end
    
    clusterer = Clusterer(varargin{:});
    
    clustering_difference = [];
    clustering_time = [];
    group_merges = [];
            
    if params.livePreview
        ac_plot = figure;
    end
    
    for f = files_order
        [descriptor, target_groups, time] = step(clusterer, file_names{f}, 'file', file_names{f}, 'original_label', label_from_filename(file_names{f}));
        group_merges(end+1) = max(0, length(target_groups)-1);
        clustering_time(end+1) = time;
        
        non_zero_descriptors_index = find([clusterer.descriptors.original_label] ~= 0);
        clustering_difference(end+1) = clusters_match([clusterer.descriptors(non_zero_descriptors_index).label], [clusterer.descriptors(non_zero_descriptors_index).original_label]);

        if params.livePreview
            figure(ac_plot);
            plot_clusterings_difference(clustering_difference, group_merges);
            title({'clustering error (less is better, group zero excluded)', struct2label(params, 'wrap', 3)});
            drawnow;
        end
    end
    
    if params.livePreview
        close(ac_plot);
    end
    
    delete(sprintf('%s/*/*', data_path));
    for d = 1:length(clusterer.descriptors)
        dir_path = sprintf('%s/%d', data_path, clusterer.descriptors(d).label);
        if ~exist(dir_path, 'dir')
            mkdir(dir_path);
        end
        [~,basename,ext] = fileparts(clusterer.descriptors(d).file);
        target_file = sprintf('%s/%s%s', dir_path, basename, ext);
        copyfile(clusterer.descriptors(d).file, target_file);
    end
    
    f=figure('Visible', 'off');
    plot_clusterings_difference(clustering_difference, group_merges);
    title({'clustering error (less is better, group zero excluded)', struct2label(params, 'wrap', 3)});
    saveas(f, sprintf('%s/clustering-error.svg', data_path));
    close(f);
    
    f=figure('Visible', 'off');
    plot(clustering_time)
    title({'clustering time'});
    xlabel('time [s]');
    ylabel('number of elements in memmory');
    saveas(f, sprintf('%s/clustering-time.svg', data_path));
    close(f);
    
    if nargout-1 >= 1
        varargout{1} = clustering_difference;
    end
    
    if nargout-1 >= 2
        varargout{2} = group_merges;
    end
    
    if nargout-1 >= 3
        varargout{3} = clustering_time;
    end
    
    descriptors = clusterer.descriptors;
    save(sprintf('%s/descriptors', data_path), 'descriptors', 'clustering_difference', 'group_merges', 'clustering_time');
end