init;

camera = ipcam('http://172.31.31.119:8080/video');
% camera = webcam(1);

classifier = Clusterer('groupDiscardThreshold', 10);

preview = vision.VideoPlayer('Position', [20 100 1300 800]);

faceDetector = vision.CascadeObjectDetector();
detection_times = [];

while true % ~isDone(camera)
    start = tic;
    frame = snapshot(camera);
    
    faces = step(faceDetector, frame); % [siftx y w h; x y w h; ...]
    detection_times(end+1) = toc(start);
    labels = [];
    colors = {};
    
    if ~isempty(faces)
        for f = 1:size(faces, 1)
            [details, ~, ~, is_persisted] = step(classifier, imcrop(frame, faces(f, :)));
            labels(f) = details.label;
            if is_persisted
                colors{f} = 'green';
            else
                colors{f} = 'red';
            end
        end
        frame = insertObjectAnnotation(frame, 'Rectangle', faces, labels, 'Color', colors);
    end
    step(preview, frame);   
end

release(preview);
% release(grabber);
clear('camera');
