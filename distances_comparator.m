function [distances, results] = distances_comparator(file_names, descriptor_type, thumb_size, varargin)
    parser = inputParser;
    addParameter(parser, 'SameGroupTriming', 0, @isPositiveIntegerValuedNumeric);
    addParameter(parser, 'OtherGroupTriming', 0, @isPositiveIntegerValuedNumeric);
    parse(parser, varargin{:});
    params = parser.Results;

    fprintf('descriptor: %s, thumb: %dx%d, 1st file: %s\n', descriptor_type, thumb_size, thumb_size, file_names{1});

    fprintf('loading and filtering files  ... ');
    for i = 1:length(file_names)
        if mod(i, round(length(file_names)/20)) == 0
            fprintf('%d%% ', round(i/length(file_names)*100));
        end
        
        results(i).label = label_from_filename(file_names{i})
        results(i).filename = file_names{i};
        
        results(i).original_size = [0 0];
        results(i).descriptor = [];
        results(i).descriptor_generation_time = inf;
    end
    fprintf('done\n');

    results = results(find(~([results.label] == 0))); % filtering group 0
    unique_labels = unique([results.label]);

    fprintf('comparing distances ... ');
    for label_idx = 1:length(unique_labels)
        fprintf('%d%% ',round(label_idx/length(unique_labels)*100));

        % selecting items' indexes
        same_group_indexes   = find([results.label] == unique_labels(label_idx));
        remain_group_indexes = find(~([results.label] == unique_labels(label_idx)));

        % little cuttung of results
        if params.SameGroupTriming
            same_group_indexes   = same_group_indexes(randperm(length(same_group_indexes), min(params.SameGroupTriming, length(same_group_indexes))));
        end

        if params.OtherGroupTriming
            remain_group_indexes = remain_group_indexes(randperm(length(remain_group_indexes), min(params.OtherGroupTriming, length(remain_group_indexes))));
        end

        same_group_distances = [];
        remain_group_distances = [];

        for i = 1:length(same_group_indexes)
            results(same_group_indexes(i)) = lazy_load_descriptor(results(same_group_indexes(i)), descriptor_type, thumb_size);
            for j = 1:length(same_group_indexes)
                if i ~= j
                    results(same_group_indexes(j)) = lazy_load_descriptor(results(same_group_indexes(j)), descriptor_type, thumb_size);
                    val = distance_between(results(same_group_indexes(i)).descriptor, results(same_group_indexes(j)).descriptor, descriptor_type);
                    same_group_distances = [same_group_distances val];
                end
            end

            for j = 1:length(remain_group_indexes)
                results(remain_group_indexes(j)) = lazy_load_descriptor(results(remain_group_indexes(j)), descriptor_type, thumb_size);
                val = distance_between(results(same_group_indexes(i)).descriptor, results(remain_group_indexes(j)).descriptor, descriptor_type);
                remain_group_distances = [remain_group_distances val];
            end
        end

        distances(label_idx).group        = unique_labels(label_idx);
        distances(label_idx).insides      = same_group_distances;
        distances(label_idx).outsides     = remain_group_distances;
        distances(label_idx).inside_mean  = mean(same_group_distances);
        distances(label_idx).outside_mean = mean(remain_group_distances);
        distances(label_idx).inside_std   = std(same_group_distances);
        distances(label_idx).outside_std  = std(remain_group_distances);
    end
    fprintf('done\n');
end
