function distances = distances_compare_batch(images_path, output_data_dir, descriptor_types, thumb_sizes)
    file_names = images_list_from(images_path);

    for e = 1:length(descriptor_types)
        for t = 1:length(thumb_sizes)
            thumb_size = thumb_sizes(t);
            [group_distances, results] = distances_comparator(file_names, descriptor_types{e}, thumb_size, 'SameGroupTriming', 6, 'OtherGroupTriming', 50);
            % distances_comparator(file_names, distances(e).extractor, thumb_size, 'SameGroupTriming', 6, 'OtherGroupTriming', 50);

            distances_save(group_distances, output_data_dir, descriptor_types{e}, thumb_size);

            loaded_results_indexes = find([results.descriptor_generation_time] ~= inf);

            distances(e).set(t).inside_means    = [group_distances.inside_mean];
            distances(e).set(t).outside_means   = [group_distances.outside_mean];
            distances(e).set(t).thumb_size      = thumb_size;
            distances(e).set(t).desc_gen_mean   = mean([results(loaded_results_indexes).descriptor_generation_time]);
            distances(e).set(t).desc_gen_std    = std([results(loaded_results_indexes).descriptor_generation_time]);
            distances(e).set(t).upscale_ratio   = sum(cell2mat(arrayfun(@(r) all(r.original_size(1:2) < [thumb_size thumb_size]), results(loaded_results_indexes), 'UniformOutput', false)))/length(loaded_results_indexes);
            distances(e).set(t).downscale_ratio = sum(cell2mat(arrayfun(@(r) all(r.original_size(1:2) > [thumb_size thumb_size]), results(loaded_results_indexes), 'UniformOutput', false)))/length(loaded_results_indexes);
            distances(e).set(t).midscale_ratio  = 1 - distances(e).set(t).upscale_ratio - distances(e).set(t).downscale_ratio;
        end
    end


    for e = 1:length(distances)
        distances(e).inside_means  = cell2mat(arrayfun(@(d)(mean(d.inside_means)),  distances(e).set, 'UniformOutput', false));
        distances(e).outside_means = cell2mat(arrayfun(@(d)(mean(d.outside_means)), distances(e).set, 'UniformOutput', false));
        distances(e).inside_stds   = cell2mat(arrayfun(@(d)(std(d.inside_means)),   distances(e).set, 'UniformOutput', false));
        distances(e).outside_stds  = cell2mat(arrayfun(@(d)(std(d.outside_means)),  distances(e).set, 'UniformOutput', false));

        % plot with distances
        f=figure('Visible', 'off');
        errorbar( ...
            [distances(e).set.thumb_size; distances(e).set.thumb_size]', ...
            [distances(e).inside_means; distances(e).outside_means]',    ...
            [distances(e).inside_stds;  distances(e).outside_stds]'      ...
        );
        title(sprintf('average distances for %s descriptor', descriptor_types{e}));
        xlabel('thumbnail size [px]');
        ylabel('average distance');
        legend('inside group', 'between groups');
        path = sprintf('%s/distance-change-for-%s-descriptor-vs-thumb-size.svg',output_data_dir, descriptor_types{e});
        saveas(f, path);
        close(f);

        % plot with distances difference
        f=figure('Visible', 'off');
        errorbar( ...
            [distances(e).set.thumb_size]', ...
            [distances(e).outside_means ./ distances(e).inside_means]', ...
            [distances(e).outside_stds  ./ distances(e).inside_stds]'   ...
        );
        title(sprintf('ratio out/in average distances for %s descriptor', descriptor_types{e}));
        xlabel('thumbnail size [px]');
        ylabel('average inside distance / average outside distance');
        path = sprintf('%s/distance-ratio-change-for-%s-descriptor-vs-thumb-size.svg',output_data_dir, descriptor_types{e});
        saveas(f, path);
        close(f);

        % plot with generation times
        f=figure('Visible', 'off');
        errorbar( ...
            [distances(e).set.thumb_size]',    ...
            [distances(e).set.desc_gen_mean]', ...
            [distances(e).set.desc_gen_std]'   ...
        );
        title(sprintf('average generation time %s descriptor', descriptor_types{e}));
        xlabel('thumbnail size [px]');
        ylabel('time [s]');
        path = sprintf('%s/descriptor-generation-time-for-%s-descriptor-vs-image-size.svg',output_data_dir, descriptor_types{e});
        saveas(f, path);
        close(f);

        % plot with scaling types
        f=figure('Visible', 'off');
        plot(                                                                                                    ...
            [distances(e).set.thumb_size]',                                                                      ...
            [distances(e).set.upscale_ratio; distances(e).set.midscale_ratio; distances(e).set.downscale_ratio]' ...
        );
        title('image resizing proportions distibution');
        xlabel('thumbnail size [px]');
        ylabel('resize ratio <0, 1>');
        legend('upscaled', 'only small adjustment', 'downscaled');
        path = sprintf('%s/images-resize-proportions-distribution-for-%s-extractor.svg', output_data_dir, descriptor_types{e});
        saveas(f, path);
        close(f);
    end

    save(sprintf('%s/distance-change-for-descriptors-vs-image-size',output_data_dir), 'distances');
end
