init;

thumb_sizes = [24, 32, 40, 48, 56, 64, 80, 96, 128, 160, 192, 224, 256, 288, 320]
descriptors = {'SIFT', 'LBP', 'HOG'}
inputs = {'data/faces/360-80-100 happy faces hd groupped', 'data/faces/100-80-Hannibal Full Trailer groupped', 'data/faces/att scholarship'}
outputs = {'data/distance-comparition/360-80-100 happy faces', 'data/distance-comparition/100-80-Hannibal Full Trailer', 'data/distance-comparition/att scholarship'}

parfor i = 1:length(inputs)
    distances_compare_batch(inputs{i}, outputs{i}, descriptors, thumb_sizes);
end