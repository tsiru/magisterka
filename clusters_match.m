function error = clusters_match(A, B)
  error = zeros(length(A));

  for i = 1:length(A)
    Am = (A == A(i));
    Bm = (B == B(i));
    error(i, :) = (xor(Am, Bm));
  end

  sumerror = sum(error);
  sumerror = sumerror(sumerror > 0);

  m = mean(sumerror);
  if isempty(m) || isnan(m)
    m = 0;
  end

  if all(sumerror == m)
    error = m;
  else
    error = sum(sumerror >= m);
  end
end

