function distances_save(distances, output_data_dir, extractor_name, thumb_size)
    file_descriptive_name = [num2str(thumb_size) 'x' num2str(thumb_size) '-' extractor_name];
    chart_description = ['thumb_size: ' num2str(thumb_size) 'x' num2str(thumb_size) 'px, descritor: ' extractor_name];

    mkdir(output_data_dir);
    save([output_data_dir '/distances-' file_descriptive_name], 'distances');

    f=figure('Visible', 'off');
    errorbar([distances.group; distances.group]', [distances.inside_mean; distances.outside_mean]', [distances.inside_std; distances.outside_std]')
    title({'distances between elements', chart_description});
    xlabel('group no');
    ylabel('distance');
    legend('inside group', 'inside vs outside');
    saveas(f, [output_data_dir '/distances-' file_descriptive_name '.svg']);
    close(f);
end