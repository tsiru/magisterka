classdef Grabber < handle
    properties
        frameToSkipFromStart = 0
        frameToSkipPercent = 0
        frameCount = 0
        videoFile
        videoSource
    end
    
    methods
        
        function self = Grabber(videoFile, varargin)
            self.videoFile   = videoFile;
            self.videoSource = vision.VideoFileReader(videoFile);
            self.frameCount  = 0;
            
            parser = inputParser;
            addParameter(parser, 'frameToSkipFromStart', 0, @(n) (isnumeric(n) && n >= 0));
            addParameter(parser, 'frameToSkipPercent', 0, @(n) (isnumeric(n) && n >= 0 && n < 100));
            parse(parser, varargin{:});
            
            self.frameToSkipFromStart = parser.Results.frameToSkipFromStart;
            self.frameToSkipPercent   = parser.Results.frameToSkipPercent;
        end
        
        function done = isDone(self)
            done = isDone(self.videoSource);
        end
        
        function frame = step(self)
            frame = false;
            while ~isDone(self.videoSource)
                % Get the next frame.
                frame = step(self.videoSource);
                if isempty(frame)
                    return;
                end
                self.frameCount = self.frameCount + 1;
                % fprintf('frame no %d', frameCount);

                if shouldSkipFrame(self)
                    % fprintf(' - skipped\n');
                    continue;
                end
                % fprintf(' - processing \n');
                return;
            end
        end
        
        function multiplier = frameSkipPercentageMultiplier(self)
            multiplier = nwd(self.frameToSkipPercent, 100);
        end
        
        function skip = shouldSkipFrame(self)
            if self.frameCount < self.frameToSkipFromStart 
                skip = true;
                return;
            end
            
            if mod(self.frameCount, 100/frameSkipPercentageMultiplier(self)) < self.frameToSkipPercent/frameSkipPercentageMultiplier(self)
                skip = true;
                return;
            end
            
            skip = false;
        end
        
        function release(self)
            release(self.videoSource);
            disp 'released Grabber';
        end
        
    end
    
end

