function descriptor = lazy_load_descriptor(descriptor, descriptor_type, thumb_size)
    if isinf(descriptor.descriptor_generation_time)
%         fprintf('lazy load descriptor for %s\n', descriptor.filename);
        image = imread(descriptor.filename);
        descriptor.original_size = size(image);
        start = tic;
        descriptor.descriptor = build_descriptor(image, descriptor_type, thumb_size);
        descriptor.descriptor_generation_time = toc(start);
    else
%         fprintf('descriptor lazy load hit for %s\n', descriptor.filename);
    end
end