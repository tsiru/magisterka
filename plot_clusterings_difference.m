function plot_clusterings_difference(clustering_difference, group_merges)
    plot(clustering_difference);
    for m = find(group_merges)
        line([m m], get(gca, 'ylim'), 'Color', [0.8 0.8 0.8]);
    end
    title({'clustering error', 'less is better, group zero excluded'});
    xlabel('iteration no');
    ylabel('count');
    without_warnings(@() (legend('clustering errors', 'merges between groups', 'Location', 'northwest')));
end