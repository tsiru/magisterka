function label = struct2label(str, varargin)
    parser = inputParser;
    addParameter(parser, 'wrap', inf, @(n) (isnumeric(n) && n > 0));
    parse(parser, varargin{:});
    wrap = parser.Results.wrap;
    
    label_pieces = {};
    fields = fieldnames(str);
    for i = 1:length(fields)
        value = getfield(str, char(fields{i}));
        
        if ismatrix(value)
            value = mat2str(value);
        elseif isnumeric(value)
            value = sprintf('%f', value);
        end
        
        n = sprintf('%s: %s', char(fields{i}), value);
        label_pieces{length(label_pieces)+1} = n;
    end
    
    if length(label_pieces) > wrap
        wrapps = {};
        L = length(label_pieces);
        for i = 1:wrap:L
            wrapps{length(wrapps)+1} = strjoin({label_pieces{i:min(L, i+wrap-1)}}, ', ');
        end
        label = strjoin(wrapps, '\n');
    else
        label = strjoin(label_pieces, ', ');
    end
    
    
end
