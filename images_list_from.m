function file_names = images_list_from(images_path)
    files = dir(images_path);

    % filtering to have only images
    files = files(find(~[files.isdir]));
    files = files(~cellfun(@isempty, regexp({files.name}, '.(?:jpg|jpeg|pgm)$')));

    file_names = arrayfun(@(n) ( [images_path '/' n{1}]) , {files.name}, 'UniformOutput', false);
end