% num = match(image1, image2)
%
% This function reads two images, finds their SIFT features, and
%   displays lines connecting the matched keypoints.  A match is accepted
%   only if its distance is less than distRatio times the distance to the
%   second closest match.
% It returns the number of matches displayed.
%
% Example: match('scene.pgm','book.pgm');

function num = match(descriptor1, descriptor2)
    des1 = descriptor1.descriptors;
    des2 = descriptor2.descriptors;

    % For efficiency in Matlab, it is cheaper to compute dot products between
    %  unit vectors rather than Euclidean distances.  Note that the ratio of 
    %  angles (acos of dot products of unit vectors) is a close approximation
    %  to the ratio of Euclidean distances for small angles.
    %
    % distRatio: Only keep matches in which the ratio of vector angles from the
    %   nearest to second nearest neighbor is less than distRatio.
    distRatio = 0.6;   

    % For each descriptor in the first image, select its match to second image.
    des2t = des2';                          % Precompute matrix transpose
    matches = zeros(size(des1,1), 1);
    for i = 1 : size(des1,1)
       dotprods = des1(i,:) * des2t;        % Computes vector of dot products
       [vals,indx] = sort(acos(dotprods));  % Take inverse cosine and sort results

       % Check if nearest neighbor has angle less than distRatio times 2nd.
       if length(vals) > 1 && (vals(1) < distRatio * vals(2))
          matches(i) = indx(1);
%        else
%           matches(i) = 0;
       end
    end
    num = sum(matches > 0);
end

