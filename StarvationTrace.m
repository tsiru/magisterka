classdef StarvationTrace < handle
    properties
        groups
    end
    methods
        function self = StarvationTrace()
            self.groups = [];
        end
        
        function values = get(self, groups)
            values = max(get_real(self, groups), 0);
        end
        
        function values = is_excluded(self, groups)
            values = zeros(1, length(groups));
            for g = 1:length(groups)
                if ~group_exists(self, groups(g))
                    error('StarvationTrace:NoSuchGroup', sprintf('No such group - %d', groups(g)));
                else
                    values(g) = self.groups(find(group_index(self, groups(g))), 2) < 0;
                end
            end
        end
        
        function increment_all(self)
            if ~isempty(self.groups)
                self.groups(:, 2) = self.groups(:, 2) + 1;
            end
        end
        
        function exclude(self, groups)
            set(self, groups, -inf);
        end
        
        function merge(self, target, groups_to_merge)
            set(self, target, min(get_real(self, [target groups_to_merge])));
            remove(self, groups_to_merge);
        end
        
        
        function remove(self, groups)
            for g = 1:length(groups)
                if ~group_exists(self, groups(g))
                    error('StarvationTrace:NoSuchGroup', sprintf('No such group - %d', groups(g)));
                else
                    self.groups = self.groups(find(~group_index(self, groups(g))), :);
                end
            end
        end
        
        function reset(self, groups)
            for g = 1:length(groups)
                if ~group_exists(self, groups(g))
                    error('StarvationTrace:NoSuchGroup', sprintf('No such group - %d', groups(g)));
                else
                    prev = self.groups(find(group_index(self, groups(g))), 2);
                    self.groups(find(group_index(self, groups(g))), 2) = min(prev, 0);
                end
            end
        end
        
        function add(self, groups)
            self.groups(end+1, 1:2) = [groups(:) zeros(length(groups),1)];
        end
        
        function groups = bigger_than(self, value)
            groups = self.groups(find(self.groups(:, 2) > value), 1);
            groups = groups';
        end
        
        function result = group_exists(self, group)
            result = any(group_index(self, group));
        end
    end
    
    methods(Access=private)
        function set(self, groups, value)
            for g = 1:length(groups)
                if ~group_exists(self, groups(g))
                    error('StarvationTrace:NoSuchGroup', sprintf('No such group - %d', groups(g)));
                else
                    self.groups(find(group_index(self, groups(g))), 2) = value;
                end
            end
        end
        
        function index = group_index(self, group)
            if isempty(self.groups)
                index = [];
            else
                index = (self.groups(:, 1) == group);
            end
        end
        
        function values = get_real(self, groups)
            values = zeros(1, length(groups));
            for g = 1:length(groups)
                if ~group_exists(self, groups(g))
                    error('StarvationTrace:NoSuchGroup', sprintf('No such group - %d', groups(g)));
                else
                    values(g) = self.groups(find(group_index(self, groups(g))), 2);
                end
            end
        end
    end
end