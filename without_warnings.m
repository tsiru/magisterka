function result = without_warnings(callback)
    w = warning ('off','all');
    result = callback();
    warning(w);
end