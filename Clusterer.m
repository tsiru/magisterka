classdef Clusterer < handle
    properties
        % data
        descriptors
        last_age
        last_label
        starvation_trace

        % config vars
        descriptorType
        thumbSize
        useNLastElements
        newGroupThreshold
        groupPreserveAmount
        groupDiscardThreshold
        discardStarved
    end

    methods(Static)
        function parserSetup(parser)
            addParameter(parser, 'thumbSize', 96, @isnumeric);
            addParameter(parser, 'useNLastElements', inf, @isnumeric);
            addParameter(parser, 'newGroupThreshold', -3, @isfloat);
            addParameter(parser, 'groupPreserveAmount', 5, @isfloat);
            addParameter(parser, 'groupDiscardThreshold', 30, @isfloat);
            addParameter(parser, 'discardStarved', true, @islogical);
            addParameter(parser, 'descriptorType', 'SIFT', @ischar);
        end
    end
    
    methods
        function self = Clusterer(varargin)
            parser = inputParser;
            parser.KeepUnmatched = true;
            Clusterer.parserSetup(parser);
            parser.parse(varargin{:});
            parser.Results

            self.descriptorType        = parser.Results.descriptorType;
            self.thumbSize             = parser.Results.thumbSize;
            self.useNLastElements      = parser.Results.useNLastElements;
            self.newGroupThreshold     = parser.Results.newGroupThreshold;
            self.groupPreserveAmount   = parser.Results.groupPreserveAmount;
            self.groupDiscardThreshold = parser.Results.groupDiscardThreshold;
            self.discardStarved        = parser.Results.discardStarved;
            
            self.starvation_trace = StarvationTrace;
            self.last_age         = 0;
            self.last_label       = 0;
        end
        
        function [details, varargout] = step(self, image, varargin)
            descriptor = build_descriptor(image, self.descriptorType, self.thumbSize);
            target_labels = [];
            start_time = tic;

            if ~isempty(self.descriptors)
                labels = unique([self.descriptors.label]);

                labels_matches = inf(size(labels));
                for l = 1:length(labels)
                    if ~self.discardStarved && group_is_starved(self, labels(l))
                        continue;
                    end
                    
                    solidify_group_if_met_requirements(self, labels(l));
                    labels_matches(l) = distance_to_group(self, descriptor, labels(l));
                end

                target_labels = labels(find(labels_matches < self.newGroupThreshold));

                if length(target_labels) > 1
                    merge_groups(self, target_labels(1), target_labels(2:end));
                end

                increment_all(self.starvation_trace);
                
                if any(target_labels)
                    value = labels_matches(labels_matches < self.newGroupThreshold);
                    fprintf('matches "%s" to group %d (first %f)\n', 'unknown', target_labels(1), value(1));
                    reset(self.starvation_trace, target_labels(1));
                else
                    target_labels(1) = new_label(self);
                    fprintf('matches "%s" to new group %d (best %f)\n', 'unknown', target_labels(1), min(labels_matches));
                    add(self.starvation_trace, target_labels(1));
                end
                
                label = target_labels(1);
                
                % TODO - group for label prouning and internal consistency check
                % TODO - group ttl vs items check for other groups
            else
                label = new_label(self);
                add(self.starvation_trace, label);
            end
            details = add_descriptor(self, descriptor, label, varargin{:});
            
            discard_starved(self);
            
            if nargout >= 1
                varargout{1} = target_labels;
            end

            if nargout >= 2
                varargout{2} = toc(start_time);
            end

            if nargout >= 3
                varargout{3} = is_excluded(self.starvation_trace, label);
            end
        end
        
        function last_descriptor = add_descriptor(self, descriptor, label, varargin)
            last_descriptor = struct('descriptor', descriptor, 'label', label, 'born', new_age(self), 'scores', 0, varargin{:});
            if(isempty(self.descriptors))
                self.descriptors = last_descriptor;
            else
                self.descriptors(end+1) = last_descriptor;
            end
        end

        function age = new_age(self)
            self.last_age = self.last_age + 1;
            age = self.last_age;
        end
        
        function age = new_label(self)
            self.last_age = self.last_age + 1;
            age = self.last_age;
        end        
        
        function merge_groups(self, target_group, groups_to_merge)
            for l = groups_to_merge
                fprintf('merging group %d -> %d\n', l, target_group);
                for k = find([self.descriptors.label] == l)
                    self.descriptors(k).label = target_group;
                end
            end
            merge(self.starvation_trace, target_group, groups_to_merge);
        end
        
        function starved = group_is_starved(self, group_label)
            starved = get(self.starvation_trace, group_label) >= self.groupDiscardThreshold;
        end
        
        function solidify_group_if_met_requirements(self, group_label)
            if ~is_excluded(self.starvation_trace, group_label) && sum([self.descriptors.label] == group_label) >= self.groupPreserveAmount
                fprintf('solidified group %d\n', group_label);
                exclude(self.starvation_trace, group_label);
            end
        end
        
        function descriptors = best_descriptors_for_label(self, group_label)
            indexes = find([self.descriptors.label] == group_label);
            indexes = indexes(max(1,end-self.useNLastElements):end);
            descriptors = self.descriptors(indexes);
        end
        
        function discard_starved(self)
            if ~self.discardStarved
                return
            end
            
            groups_to_discard = bigger_than(self.starvation_trace, self.groupDiscardThreshold);
            if isempty(groups_to_discard)
                return
            end
            
            fprintf('going to discard %d\n', groups_to_discard);
            remove(self.starvation_trace, groups_to_discard);
            indices_to_discard = zeros(size(self.descriptors));            
            for g = groups_to_discard
                indices_to_discard = indices_to_discard | ([self.descriptors.label] == g);
            end
            self.descriptors = self.descriptors(find(~indices_to_discard));
        end

        function distance = distance_to_group(self, descriptor, group_label)
            same_group_descriptors = best_descriptors_for_label(self, group_label);
            same_group_distances = inf(size(same_group_descriptors));
            
            for s = 1:length(same_group_descriptors)
                same_group_distances(s) = distance_between(same_group_descriptors(s).descriptor, descriptor, self.descriptorType);
            end
            
            distance = mean(same_group_distances);
        end
    end
end
