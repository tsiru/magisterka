init;

% 0.00125 hog
% 0.0015 lbp

input_dir = 'data/faces';
output_dir = 'data/clustering';

inputs = {'360-80-100 happy faces hd groupped', '100-80-Hannibal Full Trailer groupped', 'att scholarship'};

clustering_differences = [];
clustering_times = [];

useLastNElements_variants = [1 2 3 4 5 6 7 10 15 20 30 inf]

for li = 1:length(useLastNElements_variants)
    n = useLastNElements_variants(li);
    current_data_dir = sprintf('%s/%s-last-%03d', output_dir, inputs{1}, n);
     [descriptors, clustering_difference, group_merges, clustering_time] = ... 
        clustering_batch_load_cache(                ...
            sprintf('%s/%s', input_dir, inputs{1}), ...
            current_data_dir,                       ...
            'removeZeroLabel', false,               ...
            'shuffleFiles', false,                  ...
            'useNLastElements', n,                  ...
            'livePreview', false,                   ...
            'discardStarved', false                 ...
        );
    clustering_differences = [clustering_differences; clustering_difference];
    clustering_times = [clustering_times; clustering_time];
end

f = figure;
plot(clustering_differences')
title('clustering error in relation to useNLastElements parameter');
xlabel('number of iteration');
ylabel('errors');
legend(cellstr(num2str(useLastNElements_variants', 'N=%-d')), 'Location', 'northwest');
saveas(f, 'data/clustering/clustering-error-vs-useNLastElements.svg');
csvwrite('data/clustering/clustering-error-vs-useNLastElements.csv', [useLastNElements_variants; clustering_differences']);

f = figure;
bar(mean(clustering_differences')); 
set(gca,'xticklabel',cellstr(num2str(useLastNElements_variants', 'N=%-d')));
title('average clustering error in relation to useNLastElements parameter');
saveas(f, 'data/clustering/clustering-errors-mean-vs-useNLastElements.svg');


f = figure;
plot(clustering_times')
title('clustering time in relation to useNLastElements parameter');
xlabel('number of iteration');
ylabel('time [s]');
legend(cellstr(num2str(useLastNElements_variants', 'N=%-d')), 'Location', 'northwest');
saveas(f, 'data/clustering/clustering-time-vs-useNLastElements.svg');
csvwrite('data/clustering/clustering-time-vs-useNLastElements.csv', [useLastNElements_variants; clustering_times']);

f = figure;
bar(mean(clustering_times')); 
set(gca,'xticklabel',cellstr(num2str(useLastNElements_variants', 'N=%-d')));
title('average clustering time in relation to useNLastElements parameter');
saveas(f, 'data/clustering/clustering-times-mean-vs-useNLastElements.svg');

% parfor i = 1:length(inputs) 
%     for remove_zero_label = [true false]
%         for shuffle_files = [true false]
%             input  = sprintf('%s/%s', input_dir, inputs{i});
%             output = sprintf('%s/%s', output_dir, inputs{i});
%             
%             if shuffle_files
%                 output = sprintf('%s-shuffled', output);
%             end
%             
%             if remove_zero_label
%                 output = sprintf('%s-without-noise', output);
%             end
%             
%             clustering_batch(                          ...
%                 input, output,                         ...
%                 'removeZeroLabel', remove_zero_label,  ...
%                 'shuffleFiles', shuffle_files,         ...
%                 'livePreview', false,                  ...
%                 'discardStarved', false                ...
%             );
%         end
%     end
% end