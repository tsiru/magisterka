function distance = distance_between(descriptorA, descriptorB, type)
    switch type
        case 'LBP'
            distance = sqrt(sum(descriptorA - descriptorB).^2);      
        case 'HOG'
            distance = sqrt(sum(descriptorA - descriptorB).^2);
        case 'SIFT'
            distance = -match(descriptorA, descriptorB);
        otherwise
            error(sprintf('"%s" is not proper name of descriptor.', type));
    end
end
