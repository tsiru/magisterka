function descriptor = build_descriptor(image, type, thumb_size)
    if ischar(image)
        image = imread(image);
    end
    
    image = imresize(rgb2gray_ifneeded(image), [thumb_size thumb_size]);
    
    switch type
        case 'LBP'
            descriptor = extractLBPFeatures(image);        
        case 'HOG'
            descriptor = extractHOGFeatures(image);
        case 'SIFT'
            [descriptors, locs] = sift(image);
            descriptor.descriptors = descriptors;
            descriptor.locs        = locs;
        otherwise
            error(sprintf('"%s" is not proper name of descriptor.', type));
    end
    
end