function [descriptors, varargout] = clustering_batch_load_cache(images_path, data_path, varargin)
    data_file = sprintf('%s/descriptors.mat', data_path);
    if exist(data_file, 'file')
        load(data_file);
    else
        warning(sprintf('Unable to load cache for "%s", running clustering_batch()', data_path));
        [descriptors, clustering_difference, group_merges, clustering_time] = clustering_batch(images_path, data_path, varargin{:});
    end
    
    if nargout-1 >= 1
        varargout{1} = clustering_difference;
    end

    if nargout-1 >= 2
        varargout{2} = group_merges;
    end

    if nargout-1 >= 3
        varargout{3} = clustering_time;
    end
end