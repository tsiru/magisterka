results = [];

t = 0;

t = t+1;
tests(1).setA = [1 1 1 1 1];
tests(1).setB = [1 1 1 1 1];
tests(1).distance = 0;

t = t+1;
tests(t).setA = [1 1 1 1 1];
tests(t).setB = [2 2 2 2 2];
tests(t).distance = 0;

t = t+1;
tests(t).setA = [1 1 1 1 1];
tests(t).setB = [1 2 2 2 2];
tests(t).distance = 1;

t = t+1;
tests(t).setA = [1 1 1 1 1];
tests(t).setB = [2 1 2 2 2];
tests(t).distance = 1;

t = t+1;
tests(t).setA = [1 1 1 1 1];
tests(t).setB = [2 2 1 2 2];
tests(t).distance = 1;

t = t+1;
tests(t).setA = [1 1 1 1 1];
tests(t).setB = [2 2 2 1 2];
tests(t).distance = 1;

t = t+1;
tests(t).setA = [1 1 1 1 1];
tests(t).setB = [2 2 2 2 1];
tests(t).distance = 1;

t = t+1;
tests(t).setA = [1 1 1 1 1];
tests(t).setB = [2 2 1 1 2];
tests(t).distance = 2;

t = t+1;
tests(t).setA = [1 1 1 1 1];
tests(t).setB = [1 1 3 4 5];
tests(t).distance = 3;

t = t+1;
tests(t).setA = [1 1 1 1 1];
tests(t).setB = [1 5 4 3 2];
tests(t).distance = 4;

t = t+1;
tests(t).setA = [1 1 1 1];
tests(t).setB = [1 2 3 4];
tests(t).distance = 3;

t = t+1;
tests(t).setA = [1 1 1 1];
tests(t).setB = [1 2 3 2];
tests(t).distance = 2;

t = t+1;
tests(t).setA = [1 1 1 1 1 2 3 3 4 4 5];
tests(t).setB = [1 1 1 1 1 2 3 3 4 4 5];
tests(t).distance = 0;

t = t+1;
tests(t).setA = [1 1 1 1 1 2 3 3 4 4 5];
tests(t).setB = [2 2 2 2 2 3 4 4 5 5 1];
tests(t).distance = 0;

t = t+1;
tests(t).setA = [1 1 1 1 1 2 3 3 4 4 5];
tests(t).setB = [1 1 0 1 1 2 3 3 4 4 5];
tests(t).distance = 1;

t = t+1;
tests(t).setA = [1 1 1 1 1 2 3 3 4 4 5];
tests(t).setB = [1 1 5 1 1 2 3 3 4 4 5];
tests(t).distance = 1;

for i = 1:t
  value = clusters_match(tests(i).setA, tests(i).setB);
  fprintf('for clusters\n\t%s\n\t%s\nexpected distace is %d, got %d\n\n', ...
    mat2str(tests(i).setA), ...
    mat2str(tests(i).setB), ...
    tests(i).distance,      ...
    value                   ...
  );
  tests(i).result = tests(i).distance == value;

end

fprintf('test summary:\nfailed: %d\npassed: %d\ntotal: %d\n\n', ...
  length(find([tests.result] == false)), ...
  length(find([tests.result] == true)), ...
  length(tests) ...
)

if any([tests.result] == false)
  error('test failed :-(');
else
  disp('test successful :-)');
end
