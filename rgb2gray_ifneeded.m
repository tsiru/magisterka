function gray_image = rgb2gray_ifneeded(image)
    if size(image, 3)==3
        gray_image = rgb2gray(image);
    else
        gray_image = image;
    end
end