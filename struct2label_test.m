results = [];

t = 0;

t = t+1;
tests(t).args  = { struct('a', 1) };
tests(t).value = 'a: 1';

t = t+1;
tests(t).args  = { struct('a', 1, 'b', '2') };
tests(t).value = 'a: 1, b: ''2''';

t = t+1;
tests(t).args  = { struct('a', 1, 'b', '2', 'c', [3 3]) };
tests(t).value = 'a: 1, b: ''2'', c: [3 3]';

t = t+1;
tests(t).args  = { struct('a', 1, 'b', '2', 'c', [3 3]), 'wrap', 2 };
tests(t).value = 'a: 1, b: ''2''\nc: [3 3]';

for i = 1:t
  value = struct2label(tests(i).args{:});
  expected = sprintf(tests(i).value);
  fprintf('expected: >%s<\ngot:      >%s<\n\n', expected, value);
  tests(i).result = strcmp(expected, value);
end

fprintf('test summary:\nfailed: %d\npassed: %d\ntotal: %d\n\n', ...
  length(find([tests.result] == false)), ...
  length(find([tests.result] == true)), ...
  length(tests) ...
)

if any([tests.result] == false)
  error('test failed :-(');
else
  disp('test successful :-)');
end
