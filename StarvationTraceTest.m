classdef StarvationTraceTest < matlab.unittest.TestCase
     
    properties
        starvation_trace
    end
 
    methods(TestMethodSetup)
        function createStarvationTrace(testCase)
            testCase.starvation_trace = StarvationTrace;
            increment_all(testCase.starvation_trace);
            add(testCase.starvation_trace, 1);
            add(testCase.starvation_trace, 2);
            add(testCase.starvation_trace, 3);
        end
    end
    
    methods (Test)
        function testMerge(testCase)
            testCase.verifyEqual(get(testCase.starvation_trace, [1 2 3]), [0 0 0]);
            exclude(testCase.starvation_trace, 2);
            increment_all(testCase.starvation_trace);
            testCase.verifyEqual(get(testCase.starvation_trace, [1 2 3]), [1 0 1]);
            merge(testCase.starvation_trace, 1, 2);
            testCase.verifyEqual(get(testCase.starvation_trace, [1 3]), [0 1]);
            increment_all(testCase.starvation_trace);
            testCase.verifyEqual(get(testCase.starvation_trace, [1 3]), [0 2]);
            testCase.verifyError(@() get(testCase.starvation_trace, 2), 'StarvationTrace:NoSuchGroup');
        end
        
        function testIncrementAll(testCase)
            testCase.verifyEqual(get(testCase.starvation_trace, [1 2 3]), [0 0 0]);
            increment_all(testCase.starvation_trace);
            testCase.verifyEqual(get(testCase.starvation_trace, [1 2 3]), [1 1 1]);
        end
        
        function testRemove(testCase)
            remove(testCase.starvation_trace, [1 2]);
            testCase.verifyEqual(get(testCase.starvation_trace, 3), 0);
            testCase.verifyError(@() get(testCase.starvation_trace, 1), 'StarvationTrace:NoSuchGroup');
            testCase.verifyError(@() get(testCase.starvation_trace, 2), 'StarvationTrace:NoSuchGroup');
            testCase.verifyError(@() remove(testCase.starvation_trace, 1), 'StarvationTrace:NoSuchGroup');
            testCase.verifyError(@() remove(testCase.starvation_trace, 2), 'StarvationTrace:NoSuchGroup');
        end
        
        function testReset(testCase)
            increment_all(testCase.starvation_trace);
            testCase.verifyEqual(get(testCase.starvation_trace, [1 2 3]), [1 1 1]);
            reset(testCase.starvation_trace, [2 3]);
            testCase.verifyEqual(get(testCase.starvation_trace, [1 2 3]), [1 0 0]);
        end
        
        function testExclude(testCase)
            increment_all(testCase.starvation_trace);
            testCase.verifyEqual(get(testCase.starvation_trace, [1 2 3]), [1 1 1]);
            exclude(testCase.starvation_trace, [2 3]);
            testCase.verifyEqual(get(testCase.starvation_trace, [1 2 3]), [1 0 0]);
            increment_all(testCase.starvation_trace);
            testCase.verifyEqual(get(testCase.starvation_trace, [1 2 3]), [2 0 0]);
            reset(testCase.starvation_trace, 2);
            increment_all(testCase.starvation_trace);
            testCase.verifyEqual(get(testCase.starvation_trace, [1 2 3]), [3 0 0]);
        end
        
        function testBiggerThan(testCase)
            testCase.verifyEmpty(bigger_than(testCase.starvation_trace, 0));
            increment_all(testCase.starvation_trace);
            testCase.verifyEqual(bigger_than(testCase.starvation_trace, 0), [1 2 3]);
        end
    end

end
