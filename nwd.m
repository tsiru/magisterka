function wynik = nwd(a, b)
    if (a == 0)
        wynik = b;
    else
        wynik = nwd(b, mod(a, b));
    end
end 