% [descriptors, locs] = sift(imageFile)
%
% This function reads an image and returns its SIFT keypoints.
%   Input parameters:
%     imageFile: the file name for the image.
%
%   Returned:
%     image: the image array in double format
%     descriptors: a K-by-128 matrix, where each row gives an invariant
%         descriptor for one of the K keypoints.  The descriptor is a vector
%         of 128 values normalized to unit length.
%     locs: K-by-4 matrix, in which each row has the 4 values for a
%         keypoint location (row, column, scale, orientation).  The 
%         orientation is in the range [-PI, PI] radians.
%
% Credits: Thanks for initial version of this program to D. Alvaro and 
%          J.J. Guerrero, Universidad de Zaragoza (modified by D. Lowe)

function [descriptors, locs] = sift(image)
    image = rgb2gray_ifneeded(image);
    
    image_temp_file = sprintf('%s.pgm', tempname);
    keys_temp_file  = tempname;
    
    imwrite(image, image_temp_file);

    % Call keypoints executable
    if isunix
        command = './siftUnix 2> /dev/null';
    else
        command = 'siftWin32 2>nul';
    end
    
    system(sprintf('%s < %s > %s', command, image_temp_file, keys_temp_file));
    
    % image file is no longer needed
    delete(image_temp_file);
    
    % Open tmp.key and check its header
    g = fopen(keys_temp_file, 'r');
    if g == -1
        error(sprintf('Could not open file %s.', keys_temp_file));
    end
    [header, count] = fscanf(g, '%d %d', [1 2]);
    if count ~= 2
        error('Invalid keypoint file beginning.');
    end
    num = header(1);
    len = header(2);
    if len ~= 128
        error('Keypoint descriptor length invalid (should be 128).');
    end

    % Creates the two output matrices (use known size for efficiency)
    locs = double(zeros(num, 4));
    descriptors = double(zeros(num, 128));

    % Parse tmp.key
    for i = 1:num
        [vector, count] = fscanf(g, '%f %f %f %f', [1 4]); %row col scale ori
        if count ~= 4
            error('Invalid keypoint file format');
        end
        locs(i, :) = vector(1, :);

        [descrip, count] = fscanf(g, '%d', [1 len]);
        if (count ~= 128)
            error('Invalid keypoint file value.');
        end
        % Normalize each input vector to unit length
        descrip = descrip / sqrt(sum(descrip.^2));
        descriptors(i, :) = descrip(1, :);
    end
    fclose(g);
    delete(keys_temp_file);
end

