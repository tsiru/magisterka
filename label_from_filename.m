function label = label_from_filename(filename)
    [a,b,c,d,matches] = regexp(filename, '/(?:\d+)-(\d+)(?:-[a-z0-9]+)?.(?:[a-z0-9]{3,4})$');
    label = str2num(matches{1}{1});
end